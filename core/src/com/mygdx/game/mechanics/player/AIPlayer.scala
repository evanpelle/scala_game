package com.mygdx.game.mechanics.player

import com.mygdx.game.game_action.action.{AttackAction, WalkAction}
import com.mygdx.game.game_action.event.GameEventManager
import com.mygdx.game.game_map.{Location, GameMap, IGameMap}
import com.mygdx.game.mechanics.{AIHelper, TurnManager}
import com.mygdx.game.mechanics.map_traversal.{MapTraversal, WalkTile, WalkPath}
import com.mygdx.game.tile.tile_traits.{Combatable, Moveable, Ownable}
import com.mygdx.game.tile.{ITile, Person}

/**
  * Created by evan on 4/7/16.
  */
class AIPlayer(name: String, color: String, val gameMap: IGameMap, gem: GameEventManager) extends IPlayer(name, color) {

  val helper = new AIHelper()


  override def onTurnStart(turnManager: TurnManager): Unit = {
    val opponent = turnManager.players.filter(_.color != this.color).head
    //println(s"opponent prop: ${opponent.name}")
    val people = this.property.filter(_.isInstanceOf[Person])
    if(people.isEmpty) {println("no person found"); return}
    val person = people.head.asInstanceOf[Person]

    val target = helper.findNearestPerson(person.loc, opponent)


    val (isAtLocation, hasMoved) = goToLocation(person, target.get.loc)
    if(isAtLocation) {
      val t = target.get.asInstanceOf[ITile with Combatable]
      new AttackAction(person, t, gameMap, gem).execute
    }


    turnManager.nextTurn()
  }

  def goToLocation(toMove: Person, target: Location): (Boolean, Boolean) = {
    var isAtLocation = false
    var hasMoved = false
    val mt = new MapTraversal(gameMap)
    mt.AStar(toMove, target) match {
      case Some(path) => {
        val p = helper.toWalkablePath(toMove, path)
        new WalkAction(toMove, p, gameMap, gem).execute
        hasMoved = true
      }
      case None => Unit
    }

    if(mt.reachedGoal(toMove, target)) isAtLocation = true
    (isAtLocation, hasMoved)
  }



}
