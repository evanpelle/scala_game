package com.mygdx.game.mechanics

import com.mygdx.game.game_map.Location
import com.mygdx.game.mechanics.map_traversal.WalkPath
import com.mygdx.game.mechanics.player.IPlayer
import com.mygdx.game.tile.tile_traits.{Moveable, Ownable}
import com.mygdx.game.tile.{ITile, Person}

/**
  * Created by evan on 4/28/16.
  * ai helper functions
  */
class AIHelper {


  def toWalkablePath(toWalk: ITile with Moveable, walkPath: WalkPath) = {
    new WalkPath(walkPath.path.takeWhile(toWalk.move.moveLeft >= _.pathEffort))
  }


  def findNearestPerson(loc: Location, owner: IPlayer): Option[ITile with Ownable] = {
    var closest: ITile with Ownable = null
    owner.property
      .foreach { p =>
        if(closest == null) closest = p
        else if(loc.distance(p.loc) < loc.distance(closest.loc)) {
          closest = p
        }
      }
    Option(closest)
  }

}
