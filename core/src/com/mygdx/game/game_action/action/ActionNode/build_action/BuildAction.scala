package com.mygdx.game.game_action.action.ActionNode.build_action

import com.mygdx.game.game_action.action.ActionNode.ActionNode
import com.mygdx.game.game_action.action.GameAction
import com.mygdx.game.game_action.event.{EventName, GameEventManager}
import com.mygdx.game.game_map.{Layer, IGameMap}
import com.mygdx.game.game_map.event.GameEvent
import com.mygdx.game.game_map.tile.TileFactory
import com.mygdx.game.tile.Tile
import com.mygdx.game.tile.Tile.Name
import com.mygdx.game.tile.{Tile, ITile}
import com.mygdx.game.tile.tile_traits.{Constructable, Ownable}

/**
  * Created by evan on 3/7/16.
  */
class BuildAction(builder: ITile with Constructable with Ownable,
                  val toBuildName: Name,
                  gameMap: IGameMap,
                  tf: TileFactory,
                  gem: GameEventManager) extends GameAction {


  override def execute: Boolean = {
    val toBuild = this.build
    builder.owner.resource -= toBuild.construct.resToBuild
    gameMap.setTile(toBuild, builder.loc)
    true

  }

  def isValid = this.canPlace && this.hasEnoughResToBuild


  private def canPlace: Boolean = toBuildName match {
    case Tile.city => true
    case Tile.fort => true
    case Tile.soldier => true
    case Tile.worker => true
    case Tile.mill => gameMap.getTile(builder.loc, Layer.terrain) match {
      case Some(t) => t.name == Tile.forest
      case None => false
    }
    case _ => false
  }

  private def hasEnoughResToBuild = {
    builder.owner.resource >= this.build.construct.resToBuild
  }



  private def build = toBuildName match {
    case Tile.city => tf.buildCity(builder.owner)
    case Tile.fort => tf.buildFort(builder.owner)
    case Tile.mill => tf.buildMill(builder.owner)
    case Tile.soldier => tf.buildWorker(builder.owner)
    case Tile.worker => tf.buildSoldier(builder.owner)
  }

  def getDisplayName: String = toBuildName.toString
}

object BuildAction {
  def buildNode(builder: ITile with Constructable with Ownable,
                gameMap: IGameMap, tileFactory: TileFactory, gem: GameEventManager) = {

    val bn = new ActionNode("build")
    builder.construct.canBuild
      .map(new BuildAction(builder, _ , gameMap, tileFactory, gem))
      .foreach(bn.add)
    bn
  }
}
